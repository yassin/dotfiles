#!/bin/sh
#!/bin/bash

# Define the source directory containing fonts
font_dir="dotfiles/fonts"

# Define the destination directory
destination_dir="$HOME/.local/share/"

# Create the destination directory if it doesn't exist
mkdir -p "$destination_dir"

# Copy all font files from the source directory to the destination directory
cp -R "$font_dir"/* "$destination_dir"

# Update the font cache
fc-cache -fv

# Print a message indicating successful installation
echo "Fonts installed successfully!"

