# Base Workstation configuration
{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  # Use grub as the bootloader
  boot.loader = {
  efi = {
    canTouchEfiVariables = true;
    efiSysMountPoint = "/boot/efi"; # ← use the same mount point here.
  };
  grub = {
     efiSupport = true;
     #efiInstallAsRemovable = true; # in case canTouchEfiVariables doesn't work for your system
     device = "nodev";
     useOSProber = true;
   };
  };

  # Use NetworkManager for networking
  networking.networkmanager.enable = true;

  # Enable CUPS for printing support
  # services.printing.enable = true;
  
  # Enable sound with pipewire.    
  sound.enable = true;    
  hardware.pulseaudio.enable = false;    
  security.rtkit.enable = true;    
  services.pipewire = {    
    enable = true;    
    alsa.enable = true;    
    alsa.support32Bit = true;    
    pulse.enable = true;    
    jack.enable = true;    
  }; 

}

