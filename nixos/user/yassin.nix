# User settings, applications and preferences
{ config, pkgs, ... }:

{
  users.users.yassin = {
    isNormalUser = true;
    description = "yassin";
    extraGroups = [ 
      "networkmanager" 
      "wheel" 
      "audio" 
      "video" 
      ];
    };
    # Fonts
    fonts.fonts = with pkgs; [
      (nerdfonts.override { fonts = [ "CascadiaCode" "JetBrainsMono" ]; })
    ];

    # Session variables
    environment.sessionVariables = rec {
      EDITOR          = "nvim";
    };

    # Disable sudo password prompt
    security = {
      sudo.wheelNeedsPassword = false;
    };

    # Change default shell to fish
    programs.fish.enable = true;
    users.defaultUserShell = pkgs.fish;
    # Enable Light 
    programs.light.enable = true;
    # Change /bin/sh to dash
    environment.binsh = "${pkgs.dash}/bin/dash";

    # User applications
    environment.systemPackages = with pkgs; [
      # Personal applications
      neofetch
      discord
      neovim

      # Development
      git
      gcc
      nodejs
      nodePackages.npm
      # le mega cringe
      vscode
    ];

}
