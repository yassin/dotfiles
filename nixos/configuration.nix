# Master configuration file
{ config, pkgs, ... }:
let
  # Define user in ./username.nix
  # User should have corrsponding [user].nix file
  user = import ./user/username.nix;
in
{
  imports = [ 
    # User specific options
    ./user/${user}.nix
    # Import Hyprland
    ./system/workstation/hyprland.nix
  ];

  nixpkgs.overlays = [
    (import ./overlays/waybar.nix)
  ];

  networking.hostName = "nixos";  

  i18n.defaultLocale = "en_US.utf8";
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.utf8";
    LC_IDENTIFICATION = "de_DE.utf8";
    LC_MEASUREMENT = "de_DE.utf8";
    LC_MONETARY = "de_DE.utf8";
    LC_NAME = "de_DE.utf8";
    LC_NUMERIC = "de_DE.utf8";
    LC_PAPER = "de_DE.utf8";
    LC_TELEPHONE = "de_DE.utf8";
    LC_TIME = "de_DE.utf8";
  };

  time.timeZone = "Europe/Berlin";

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Enable automatic updates
  system.autoUpgrade = {
    enable = true;
    channel = "https://nixos.org/channels/nixos";
  };

  # System version
  system.stateVersion = "22.05";

}
